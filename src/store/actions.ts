import State from '@/store/state';
import { ActionTree } from 'vuex';
import { IPackageDialogState } from '@/models/Store';

const actions: ActionTree<State, State> = {
  updateDialogState ({ commit }, { isDialogActive, packageData }: IPackageDialogState) {
    commit('setDialogState', isDialogActive);
    commit('setSelectedPackage', packageData);
  }
};

export default actions;
