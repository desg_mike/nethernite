import { GetterTree } from 'vuex';
import { IPackage } from '@/models';
import State from '@/store/state';

const getters: GetterTree<State, State> = {
  isDialogActive (state): boolean {
    return !!state.isPackageDialogActive;
  },

  selectedPackageData (state): IPackage | null {
    return state.packageData;
  }
};

export default getters;
