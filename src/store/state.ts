import { IPackage } from '@/models';

export default class State {
  isPackageDialogActive = false;
  packageData: IPackage | null = null;
}
