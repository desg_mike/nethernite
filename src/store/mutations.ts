import State from '@/store/state';
import { MutationTree } from 'vuex';
import { IPackage } from '@/models';

const mutations: MutationTree<State> = {
  setDialogState (state: State, isDialogActive: boolean) {
    state.isPackageDialogActive = isDialogActive;
  },

  setSelectedPackage (state: State, packageData?: IPackage | null) {
    state.packageData = packageData || null;
  }
};
export default mutations;
