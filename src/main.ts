import '@/assets/style.scss';
import '@mdi/font/css/materialdesignicons.css';

import Vue from 'vue';
import App from './App.vue';
import store from './store';
import vuetify from '@/plugins/vuetify';
import api from '@/plugins/api';

Vue.config.productionTip = false;

Vue.use(api);

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app');
