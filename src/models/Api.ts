export interface IAPIResponse<T> {
  objects: T[];
  time: string;
  total: number;
}
