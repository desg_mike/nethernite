export interface IField {
  label: string;
  placeholder: string;
  clearable: boolean;
  outlined: boolean;
  hideDetails: boolean;
}

export interface IMaintainer {
  email: string;
  username: string;
}

export interface IAuthor extends Partial<IMaintainer>{
  name?: string;
  url?: string;
}

export interface IPackageLinks {
  bugs?: string;
  homepage?: string;
  npm?: string;
  repository?: string;
}

export interface IPackage {
  author?: IAuthor;
  date: string;
  description: string;
  keywords?: string[];
  links: IPackageLinks;
  maintainers: IMaintainer[];
  name: string;
  publisher: IMaintainer;
  scope: string;
  version: string;
}

export interface IScoreDetail {
  maintenance: number;
  popularity: number;
  quality: number;
}

export interface IScore {
  detail: IScoreDetail;
  final: number;
}

export interface IPackageSummary {
  package: IPackage;
  score: IScore;
  searchScore: number;
}
