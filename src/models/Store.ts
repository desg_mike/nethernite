import { IPackage } from '@/models/index';

export interface IPackageDialogState {
  isDialogActive: boolean,
  packageData?: IPackage;
}
