import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { IAPIResponse } from '@/models/Api';

type RequestOptions = Pick<AxiosRequestConfig, 'url' | 'method' | 'params'>

export class Api {
  private readonly axiosInstance: AxiosInstance;

  protected constructor () {
    this.axiosInstance = axios.create({
      baseURL: process.env.VUE_APP_API_ENDPOINT
    });
  }

  protected async execute<T> (options: RequestOptions): Promise<IAPIResponse<T>> {
    const method = options.method || 'get';

    const requestParams: AxiosRequestConfig = {
      url: options.url,
      method,
      [['get', 'delete'].includes(method) ? 'params' : 'data']: options.params
    };

    const response = await this.axiosInstance.request(requestParams);
    return response.data;
  }
}
