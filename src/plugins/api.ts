import _Vue from 'vue';
import { Api as BaseApi } from '@/services/Api';
import { IAPIResponse } from '@/models/Api';
import { IPackageSummary } from '@/models';

export class Api extends BaseApi {
  private static instance: Api;

  // eslint-disable-next-line
  private constructor() {
    super();
  }

  public static getInstance (): Api {
    return Api.instance ?? (Api.instance = new Api());
  }

  async searchPackages (text: string, from: number): Promise<IAPIResponse<IPackageSummary>> {
    return await this.execute<IPackageSummary>({
      url: 'search',
      method: 'get',
      params: {
        text,
        from,
        size: 10
      }
    });
  }
}

export default function apiPlugin (Vue: typeof _Vue): void {
  Vue.prototype.$api = Api.getInstance();
}
