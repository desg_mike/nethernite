module.exports = {
  root: true,

  env: {
    node: true
  },

  parserOptions: {
    ecmaVersion: 2020
  },

  rules: {
    quotes: [
      'error',
      'single',
      {
        avoidEscape: true
      }
    ],
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-undef': 'off',
    'max-len': [
      'error',
      {
        code: 120
      }
    ],
    semi: [
      'error',
      'always'
    ],
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off'
  },

  extends: [
    'plugin:vue/essential',
    '@vue/standard',
    '@vue/typescript/recommended'
  ]
};
